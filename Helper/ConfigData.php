<?php
namespace Magenest\OrderUpload\Helper;
use Magento\Framework\App\Helper\Context;
class ConfigData extends \Magento\Framework\App\Helper\AbstractHelper
{

//    protected $logger;

    public function __construct(
        Context $context
    ) {

        parent::__construct($context);
    }


//    public function getVpcVersion()
//    {
//        return $this->vpcVersion;
//    }
//
//    public function getTitle()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/magenest_anz/anz_server_hosted/title',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//    public function getVirtualPaymentClientURL()
//    {
//        return $this->virtualPaymentClientURL;
//    }
//
//    public function getMerchantId()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/magenest_anz/vpcMerchant',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//    public function getVpcAccessCode()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/magenest_anz/vpcAccessCode',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//    public function getCanDebug()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/magenest_anz/debug',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//    public function getEnable3ds()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/anz_merchant_hosted/threeds',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//    public function getSecureSecret()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/magenest_anz/secureSecret',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//    public function getVpcUser()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/magenest_anz/vpcUser',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//    public function getVpcPassword()
//    {
//        return $this->scopeConfig->getValue(
//            'payment/magenest_anz/vpcPassword',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
//
//
//    public function getVpc_Locale()
//    {
//        return $this->scopeConfig->getValue(
//            'general/locale/code',
//            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
//        );
//    }
    public function  getIsEnabled()
    {
        return $this->scopeConfig->getValue(
            'orderupload/config/is_enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function  getExtension()
    {
        return $this->scopeConfig->getValue(
            'orderupload/config/extension',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function  getMaxSize()
    {
        return $this->scopeConfig->getValue(
            'orderupload/config/maxsize',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getMaxFiles()
    {
        return $this->scopeConfig->getValue(
            'orderupload/config/maxfiles',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
