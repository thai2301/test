<?php

namespace Magenest\Cuongnt\Block\Adminhtml\Vendor;

class AddBlock extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_vendor_add';
        $this->_blockGroup = 'Magenest_Cuongnt';
        parent::_construct();
    }

    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Add New Vendor'));
    }

}