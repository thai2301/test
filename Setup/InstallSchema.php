<?php

namespace Magenest\Cuongnt\Setup;

use    Magento\Framework\Setup\InstallSchemaInterface;
use    Magento\Framework\Setup\ModuleContextInterface;
use    Magento\Framework\Setup\SchemaSetupInterface;
use    Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface    $setup,
                            ModuleContextInterface  $context)
    {
//        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $installer = $setup;
            $installer->startSetup();
            $connection = $installer->getConnection();
            //Install	new	database	table
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_test_vendor_cuongnt')
            )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    11,
                    ['nullable' => false],
                    'Customer ID'
                )
                ->addColumn(
                    'first_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Fisrt Name'
                )
                ->addColumn(
                    'last_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Last Name'
                )
                ->addColumn(
                    'email',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Email'
                )
                ->addColumn(
                    'company',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable'	=>	true],
                    'Company'
                )
                ->addColumn(
                    'phone_number',
                    Table::TYPE_TEXT,
                    15,
                    ['nullable' => true],
                    'Phone Number'
                )
                ->addColumn(
                    'fax',
                    Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'Fax'
                )
                ->addColumn(
                    'address',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Address'
                )->addColumn(
                    'street',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Street'
                )->addColumn(
                    'country',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Country'
                )->addColumn(
                    'city',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'City'
                )->addColumn(
                    'postcode',
                    Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'Postcode'
                )->addColumn(
                    'total_sales',
                    Table::TYPE_FLOAT,
                    11,
                    ['nullable' => true],
                    'Total Sales'
                )
                ->setComment('News Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
            $installer->endSetup();
        }
}