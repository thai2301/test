<?php
namespace Magenest\Cuongnt\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $customerSetupFactory;
    private $eavSetupFactory;


    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();
        $customerSetup->addAttribute('customer', 'cuongnt_is_approved', [
            'label' => 'Is Approved',
            'type' => 'int',
            'frontend_input' => 'number',
            'required' => false,
            'visible' => true,
            'position' => 105,
            'nullable' => false,
        ]);
        $isApproved = $customerSetup->getEavConfig()->getAttribute('customer', 'cuongnt_is_approved');
        $isApproved->setData('used_in_forms', ['adminhtml_customer']);
        $isApproved->save();
        $setup->endSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

        $eavSetup->addAttribute(
            $entityTypeId,
            'cuongnt_product_vendor',
            [
                'type' => 'int',
                'label' => 'Product Vendor',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => 'simple,virtual,downloadable,configurable'
            ]
        );
        $setup->endSetup();
    }
}
?>