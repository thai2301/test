<?php

namespace Magenest\Cuongnt\Model\ResourceModel\Vendor;
/**
 *    Upload    Collection
 */
class    Collection extends
    \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *    Initialize    resource    collection
     *
     * @return    void
     */
    public function _construct()
    {
        $this->_init('Magenest\Cuongnt\Model\Vendor',
            'Magenest\Cuongnt\Model\ResourceModel\Vendor');
        $this->_idFieldName = 'id';
    }
}